
Release notes of Taquin - an application for Ubuntu Touch

v2.1.2 (on 2024-10-19)
    - update Dutch translation thanks to Heimen Stoffels

v2.1.1 (on 2023-10-18)
    - update for ubuntu touch 20.04 (issue #14)

v2.1.0 (on 2022-03-06)
    - add game statistics (issue #9)
    - fix minor issue #12: fix tile geometry to have actual square tiles when "square" is selected
    - simplify splashscreen (issue #11)

v2.0.2 (on 2021-09-11)
    - update fr and nl translations
    - fix minor issue #10 on tile mismatch occuring sometimes on game reload or first app launch

v2.0.1 (on 2021-07-18)
    - improve robustnesss of game state saving by using a json file (issue #8)
    - fix minor issues: 
        display the final tile if game over when starting app,
        re-initialize counting of moves on new game

v2.0.0 (on 2021-05-24)
    - Add user choice of the image from other applications (issue #2, issue #7)

v1.2.0 (on 2021-02-20)
    - Add German translation thanks to Wolfgang Eder
    - Add choice of borders around tiles (issue #6)
    - Add a splashscreen (issue #5)
    - Add a secret way to help solving image games

v1.1.0 (on 2021-02-06)
    - Move a tile by sliding it instead of tapping on it (issue #1)
    - Use colors of current theme: Ambiance or Suru Dark (issue #3)
    - Fix minor layout issues (issue #4)

v1.0.1 (on 2020-12-20)
    - Add French translation thanks to Anne Onyme 017 
    - Add Dutch translation thanks to Heimen Stoffels
    - Add contributors in information page and reorgannize the page layout

v1.0.0 (on 2020-12-19)
    - First release of Taquin
